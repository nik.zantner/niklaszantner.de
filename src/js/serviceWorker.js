self.addEventListener('install', e => {
  e.waitUntil(
    caches.open('niklaszantner.de')
      .then(cache => {
        return cache.addAll([
          '/',
          '/index.html',
          '/404.html',
          '/assets/favicon.ico',
          '/assets/niklas_zantner.jpg',
          '/css/main.scss',
          '/css/font-awesome.min.css',
          '/fonts/FontAwesome.otf',
          '/fonts/fontawesome-webfont.eot',
          '/fonts/fontawesome-webfont.svg',
          '/fonts/fontawesome-webfont.ttf',
          '/fonts/fontawesome-webfont.woff',
          '/fonts/fontawesome-webfont.woff2',
          '/js/app.js',
          '/js/particles.min.js',
        ]);
      })
      .catch(errors => console.warn('Service Worker: cache not found.', errors))
  );
});