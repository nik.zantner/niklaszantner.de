# Personal Website 

Using parcel for building.

## Basic commands:

Run dev server via parcel
```
npm run dev
```

Build to `./dist`
```
npm run build
```
